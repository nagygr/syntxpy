=======================
Syntx parsing framework
=======================

This a framework that allows the description of
grammars in an EBNF-like manner exploiting the
operator overloading capabilities of Python.

Semantic actions (event handlers) can be assigned
to rules to help text processing and the AST of the
parsed text is automatically created.

If parsing fails, the framework points to the character
that caused the failure, gives the line number and the name
of the rule that failed.

Requires Python 3 or later.

-------------
Using the egg
-------------

The syntx egg can be created by issuing either:

python  setup.py bdist_egg

or 

python3 setup.py bdist_egg

and it will be created under dist/. The egg library is self contained, and
can be used in any project by moving it to the project library and
placing the following lines in the code (note that the path should be
the actual path to the lib with the correct lib name (which may depend
on the library and Python version)) e.g.:

import sys
sys.path.append("dist/syntx-1.0-py2.7.egg")
from syntx import *

or

import sys
sys.path.append("dist/syntx-1.0-py3.4.egg")
from syntx import *

