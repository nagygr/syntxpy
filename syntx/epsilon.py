# The MIT License (MIT)
# 
# Copyright (c) 2015, Gergely Nagy
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import string 
import logging

from syntx.rule import Context
from syntx.rule import ResultRange
from syntx.rule import BaseRule

class Epsilon(BaseRule):
	"""
	A rule that doesn't consume any characters and always matches.
	It is useful in a number of cases: e.g. when the closing whitespaces
	of a text need to by consumed, it can be done as: -epsilon().
	This way a full match can be tested by checking that
	result.end == len(text).
	"""
	def __init__(self):
		super(Epsilon, self).__init__("_{0}".format(self.__class__.__name__))

	def __str__(self):
		return "Operation: {0}".format(self._operation)

	def test(self, text, the_context, result, a_node):
		"""Tests for a match."""
		if the_context.position < len(text):
			logging.debug("epsilon.test {0} {1}".format(the_context.position, text[the_context.position]))
		
		result.start = the_context.position
		result.end = the_context.position

		return True

