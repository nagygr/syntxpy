# The MIT License (MIT)
# 
# Copyright (c) 2015, Gergely Nagy
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import string 
import logging

from syntx.rule import Context
from syntx.rule import ResultRange
from syntx.rule import BaseRule

class NotCRange(BaseRule):
	"""Matches one character if it is not in the character range defined by self._lower_bound and self._upper_bound."""

	def __init__(self, lower_bound, upper_bound):
		"""
		self._lower_bound -- the lower bound of the disallowed character range
		self._upper_bound -- the upper bound of the disallowed character range (this element is part of the range)
		"""
		super(NotCRange, self).__init__("_{0}".format(self.__class__.__name__))
		self._lower_bound = lower_bound
		self._upper_bound = upper_bound

	def __str__(self):
		return "Operation: {0} (allowed characters: {1}-{2})".format(self._operation, self._lower_bound, self._upper_bound)

	def test(self, text, the_context, result, a_node):
		"""Tests for a match."""
		if the_context.position >= len(text):
			return False

		logging.debug("notcrange.test {0} {1} ({2}-{3})".format(the_context.position, text[the_context.position], self._lower_bound, self._upper_bound))

		local_context = Context(the_context.position)

		if not (ord(text[local_context.position]) >= ord(self._lower_bound) and ord(text[local_context.position]) <= ord(self._upper_bound)):
			the_context.position = local_context.position + 1
			result.start = local_context.position
			result.end = local_context.position + 1
			logging.debug("notcrange.result: ({0}, {1})".format(result.start, result.end))

			return True
		
		return False



