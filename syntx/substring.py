# The MIT License (MIT)
# 
# Copyright (c) 2015, Gergely Nagy
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import string 
import logging

from syntx.rule import Context
from syntx.rule import ResultRange
from syntx.rule import BaseRule

class Substring(BaseRule):
	"""Matches a given substring independently of its context."""

	def __init__(self, the_substring):
		"""self._the_substring -- the substring to match"""
		super(Substring, self).__init__("_{0}".format(self.__class__.__name__))
		self._the_substring = the_substring;

	def __str__(self):
		return "Operation: {0} (the substring: {1})".format(self._operation, self._the_substring)

	def test(self, text, the_context, result, a_node):
		"""Tests for a match."""
		local_context = Context(the_context.position)

		if local_context.position >= len(text):
			return False

		logging.debug("substring.test {0} {1}".format(the_context.position, text[the_context.position]))

		i = 0
		length = len(self._the_substring)

		while i < length and text[local_context.position + i] == self._the_substring[i]:
			i += 1

		if i == length:
			result.start, result.end = local_context.position, local_context.position + i
			the_context.position = result.end
			logging.debug("substring.result: ({0}, {1})".format(result.start, result.end))
			return True
		
		return False


