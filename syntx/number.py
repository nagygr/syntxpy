# The MIT License (MIT)
# 
# Copyright (c) 2015, Gergely Nagy
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import string 
import logging

from syntx.rule import Context
from syntx.rule import ResultRange
from syntx.rule import BaseRule

class Number(BaseRule):
	"""Matches a number (positive or negative integer or real number)."""

	def __init__(self):
		super(Number, self).__init__("_{0}".format(self.__class__.__name__))

	def __str__(self):
		return "Operation: {0} ".format(self._operation)

	def test(self, text, the_context, result, a_node):
		"""Tests for a match."""
		local_context = Context(the_context.position)
		is_real = False

		if local_context.position >= len(text):
			return False

		logging.debug("number.test {0} {1}".format(the_context.position, text[the_context.position]))

		if text[local_context.position] in "-+":
			local_context.position += 1

		if local_context.position >= len(text):
			return False

		if text[local_context.position] == "0":
			local_context.position += 1

			if local_context.position >= len(text):
				result.start = the_context.position
				result.end = local_context.position
				the_context.position = local_context.position
				return True

			if text[local_context.position] == ".":
				is_real = True
				local_context.position += 1
			elif text[local_context.position] in string.digits:
				return False

		while local_context.position < len(text) and ((text[local_context.position] in string.digits) or (text[local_context.position] == ".")):
			if text[local_context.position] == ".":
				if is_real:
					return False
				else:
					is_real = True

			local_context.position += 1

		if local_context.position > the_context.position:
			result.start = the_context.position
			result.end = local_context.position
			the_context.position = local_context.position
			logging.debug("number.result: ({0}, {1})".format(result.start, result.end))

			return True
		else:
			return False

