# The MIT License (MIT)
# 
# Copyright (c) 2015, Gergely Nagy
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import string 
import logging

from syntx.rule import Context
from syntx.rule import ResultRange
from syntx.rule import BaseRule

class Text(BaseRule):
	"""
	Matches a string enclosed by the given delimiters.
	The escape character inside the string can also be given.
	"""

	def __init__(self, delimiter = "\"", escape = "\\"):
		"""
		self._delimiter -- the delimiter character of the string
		self._escape -- the escape character of the string
		"""
		super(Text, self).__init__("_{0}".format(self.__class__.__name__))
		self._delimiter = delimiter
		self._escape = escape

	def __str__(self):
		return "Operation: {0} (delimiter: {1}, escape character: {2})".format(self._operation, self._delimiter, self._escape)

	def test(self, text, the_context, result, a_node):
		"""Tests for a match."""
		local_context = Context(the_context.position)

		if local_context.position >= len(text):
			return False

		logging.debug("text.test {0} {1} ({2}, {3})".format(the_context.position, text[the_context.position], self._delimiter, self._escape))

		if text[local_context.position] == self._delimiter:
			local_context.position += 1

			if local_context.position >= len(text):
				return False

			while text[local_context.position] != self._delimiter:
				if text[local_context.position] == self._escape:
					local_context.position += 2
				else:
					local_context.position += 1

				if local_context.position >= len(text):
					return False

			if text[local_context.position] == self._delimiter:
				result.start = the_context.position
				result.end = local_context.position + 1
				the_context.position = local_context.position + 1
				logging.debug("text.result: ({0}, {1})".format(result.start, result.end))

				return True
		
		return False
