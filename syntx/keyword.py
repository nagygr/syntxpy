# The MIT License (MIT)
# 
# Copyright (c) 2015, Gergely Nagy
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import string 
import logging

from syntx.rule import Context
from syntx.rule import ResultRange
from syntx.rule import BaseRule

class Keyword(BaseRule):
	"""
	Matches a keyword given by self._the_keyword.
	The keyword also has to be an identifier and thus
	a standalone word, so in fact first an identifier is
	consumed and then it is compared to the given keyword.
	(Nothing is really consumed if the two strings don't match.)
	"""
	def __init__(self, the_keyword, extras = ""):
		"""
		self._the_keyword -- the keyword to match
		self._extras -- the special characters that are allowed in a keyword, but do not appear in the_keyword
		"""
		super(Keyword, self).__init__("_{0}".format(self.__class__.__name__))
		self._the_keyword = the_keyword;

		self._extras = set()
		for c in self._the_keyword:
			if not (c in string.ascii_letters) and not (c in string.digits) and not (c in self._extras):
				self._extras.add(c)

		for c in extras:
			self._extras.add(c)

	def __str__(self):
		return "Operation: {0} (the keyword: {1}, extras: {2})".format(self._operation, self._the_keyword, self._extras)

	def test(self, text, the_context, result, a_node):
		"""Tests for a match."""
		local_context = Context(the_context.position)

		if local_context.position >= len(text):
			return False

		logging.debug("keyword.test {0} {1} ({2})".format(the_context.position, text[the_context.position], self._extras))

		if text[local_context.position] in string.ascii_letters or text[local_context.position] in self._extras:
			local_context.position += 1

			while local_context.position < len(text) and (text[local_context.position] in string.ascii_letters or text[local_context.position] in self._extras or text[local_context.position] in string.digits):
				local_context.position += 1

			local_result = ResultRange(the_context.position, local_context.position)
			
			if text[local_result.start : local_result.end] == self._the_keyword:
				result.start = local_result.start
				result.end = local_result.end
				the_context.position = local_context.position
				logging.debug("keyword.result: ({0}, {1})".format(result.start, result.end))
				return True
			else:
				return False
		
		return False

