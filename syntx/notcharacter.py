# The MIT License (MIT)
# 
# Copyright (c) 2015, Gergely Nagy
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import string 
import logging

from syntx.rule import Context
from syntx.rule import ResultRange
from syntx.rule import BaseRule

class NotCharacter(BaseRule):
	"""Matches one character if it is not in the character set stored in self._characters."""

	def __init__(self, characters):
		"""self._characters -- the character set that contains the invalid characters matched by this rule"""
		super(NotCharacter, self).__init__("_{0}".format(self.__class__.__name__))
		self._characters = characters

	def __str__(self):
		return "Operation: {0} (disallowed characters: {1})".format(self._operation, self._characters)

	def test(self, text, the_context, result, a_node):
		"""Tests for a match."""
		if the_context.position >= len(text):
			return False

		logging.debug("notcharacter.test {0} {1} ({2})".format(the_context.position, text[the_context.position], self._characters))

		local_context = Context(the_context.position)

		if not text[local_context.position] in self._characters:
			the_context.position = local_context.position + 1
			result.start = local_context.position
			result.end = local_context.position + 1
			logging.debug("notcharacter.result: ({0}, {1})".format(result.start, result.end))

			return True
		
		return False


