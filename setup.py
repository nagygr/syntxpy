from setuptools import setup, find_packages

setup(
    name = "syntx",
    packages = find_packages(exclude=["configuration.py", "test_syntx.py", "test_egg.py", "nmea.py", "test_nmea.py", "tester.py"]), 
    version = "1.0",
    description = "Parsing framework",
    author = "Gergely Nagy",
    author_email = "nagygr@gmail.com",
    url = "https://gitlab.com/nagygr/syntxpy",
    download_url = "https://gitlab.com/nagygr/syntxpy/repository/archive.zip",
    keywords = ["parsing", "EBNF", "grammar"],
    classifiers = [
        "Development Status :: 4 - Beta",
        "Environment :: Other Environment",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Topic :: Text Processing :: Linguistic",
        ],
    long_description = """\
Syntx parsing framework
-----------------------

This a framework that allows the description of
grammars in an EBNF-like manner exploiting the
operator overloading capabilities of Python.

Semantic actions (event handlers) can be assigned
to rules to help text processing and the AST of the
parsed text is automatically created.

If parsing fails, the framework points to the character
that caused the failure, gives the line number and the name
of the rule that failed.
"""
)
