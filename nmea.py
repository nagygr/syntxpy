import sys
sys.path.append("dist/syntx-1.0-py3.4.egg")
from syntx import *

class parse_error(Exception):
	"""Thrown when the NMEA log could not be fully parsed."""

	def __init__(self, message):
		super(parse_error, self).__init__(message)

class nmea_entry(object):
	"""Represents a single NMEA entry or sentence."""

	def __init__(self, sender = None, command = None):
		self.sender = sender
		self.command = command
		self.arguments = []

	def set_sender(self, sender):
		self.sender = sender

	def set_command(self, command):
		self.command = command

	def add_argument(self, argument):
		self.arguments.append(argument)

	def __str__(self):
		return "NMEA command: {0}\n\tsent by: {1}\n\targuments: {2}".format(self.command, self.sender, ", ".join(self.arguments))

class nmea_parser(object):
	"""An NMEA parser based on the syntx parsing library."""

	def __init__(self):
		self._entries = None

		self._log = Rule("log")
		self._nmea_sentence = Rule("nmea_sentence")
		self._sender = Rule("sender")
		self._command = Rule("command")
		self._argument = Rule("argument")

		self._log <<= +self._nmea_sentence << -Epsilon()

		self._nmea_sentence <<= ~Character("$")\
			<< self._sender(lambda match: self._entries.append(nmea_entry(match)))\
			<< self._command(lambda match: self._entries[-1].set_command(match))\
			<< +(Character(",") << self._argument(lambda match: self._entries[-1].add_argument(match)))\
			<< Option(~Character("\n"))

		self._sender <<= Substring("GP") | Substring("GN") | Substring("GL")

		self._command <<=\
			  Keyword("AAM")\
			| Keyword("ALM")\
			| Keyword("APA")\
			| Keyword("APB")\
			| Keyword("BOD")\
			| Keyword("BWC")\
			| Keyword("DTM")\
			| Keyword("GGA")\
			| Keyword("GLL")\
			| Keyword("GRS")\
			| Keyword("GSA")\
			| Keyword("GST")\
			| Keyword("GSV")\
			| Keyword("MSK")\
			| Keyword("MSS")\
			| Keyword("RMA")\
			| Keyword("RMB")\
			| Keyword("RMC")\
			| Keyword("RTE")\
			| Keyword("TRF")\
			| Keyword("STN")\
			| Keyword("VBW")\
			| Keyword("VTG")\
			| Keyword("WCV")\
			| Keyword("WPL")\
			| Keyword("XTC")\
			| Keyword("XTE")\
			| Keyword("ZTG")\
			| Keyword("ZDA")
		
		self._argument <<= Option(+(CRange("0", "9") | Character(".-*") | Identifier()))

	def parse(self, text):
		self._entries = []

		r = ResultRange()

		success = BaseRule.parse(self._log, text, r)

		if success and r.end == len(text):
			return True
		else: raise parse_error(BaseRule.get_last_error().get_error_message(text))

	def get_entries(self):
		return self._entries

