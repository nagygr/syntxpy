import sys
sys.path.append("dist/syntx-1.0-py3.4.egg")

from nmea import nmea_entry
from nmea import parse_error
from nmea import nmea_parser

import tester

def test0():
	"""Shows how the NMEA parser can be used with a simple string input."""

	text = """\
		$GPRMC,183729,A,3907.356,N,12102.482,W,000.0,360.0,080301,015.5,E*6F
		$GPRMB,A,,,,,,,,,,,,V*71
		"""
	parser = nmea_parser()
	
	try:
		if parser.parse(text):
			print("Matched:")

			for e in parser.get_entries():
				print(e)

			return True

		else: return False

	except parse_error as pe:
		print(pe)
		return False

def test1():
	"""Shows how an NMEA logfile can be processed."""

	with open("test_data/example.nmea") as nmea_file:
		text = nmea_file.read()
	
	parser = nmea_parser()
	
	try:
		if parser.parse(text):
			print("Matched:")

			for e in parser.get_entries():
				print(e)

			return True
		else: return False

	except parse_error as pe:
		print(pe)
		return False

def test2():
	"""Shows that several nmea logs can be parsed with one parser -- the parser is initialized only once."""

	text1 = "$GPRMC,183729,A,3907.356,N,12102.482,W,000.0,360.0,080301,015.5,E*6F"
	text2 = "$GPRMC,146859,A,2135.123,N,56482.465,W,050.0,586.5,456121,215.5,E*6F"
		
	parser = nmea_parser()

	try:
		s1 = parser.parse(text1)
		s2 = parser.parse(text2)

		return s1 and s2

	except parse_error as pe:
		print(pe)
		return False

def main():
	tests = [test0, test1, test2]
	t = tester.Tester(tests)

	return t.run()

main()
