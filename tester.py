# The MIT License (MIT)
# 
# Copyright (c) 2015, Gergely Nagy
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
class Tester(object):
	"""Minimalist testing framework."""

	def __init__(self, tests):
		"""self.tests -- the test functions to run (must return a boolean value)"""
		self.tests = tests

	def run(self):
		"""Runs the tests that self.tests holds."""
		test_no = 0
		failed = 0

		for test in self.tests:
			print("#########################")
			print("  Running test no. {0}".format(test_no))
			print("=========================")
			success = test()

			print("=========================")

			if not success:
				print("Test no. {0} FAILED".format(test_no))
				failed += 1
			else:
				print("Test no. {0} SUCCEEDED".format(test_no))

			test_no += 1

			print("#########################\n")

		print("#########################################")
		if failed == 0:
			print("   Every ({0}) test PASSED".format(test_no))
		else:
			print("   {0} failed out of {1} tests".format(failed, test_no))
		print("#########################################")

		if failed > 0:
			return False
		else:
			return True
