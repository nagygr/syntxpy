import sys
sys.path.append("dist/syntx-1.0-py3.4.egg")
from syntx import *

a = Rule("a")
a <<= -Keyword("hello") << -Keyword("world")

t = "hello world"
r = ResultRange()

success = BaseRule.parse(a, t, r)

if success:
	print("Matched: {0}".format(t[r.start : r.end]))
else:
	print("Didn't match. {0}".format(BaseRule.get_last_error().get_error_message(t)))
