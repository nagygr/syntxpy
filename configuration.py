# The MIT License (MIT)
# 
# Copyright (c) 2015, Gergely Nagy
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import sys
import logging

from syntx import *
import tester

class node(object):
	def __init__(self, a_value = "", parent = None):
		self.value = a_value
		self.parent = parent
		self.children = []

	def add_child(self, value = ""):
		self.children.append(node(value, self))

	def get_child(self, value):
		for child in self.children:
			if child.value == value:
				return child

		raise ValueError("{0} is not child of the current node".format(value))

	def __str__(self):
		return self.value

class configuration:
	def __init__(self, config_file):
		self.__ast = node()

		config_tree = Rule("config_tree")
		s_expression = Rule("s_expression")
		value = Rule("value")

		current = [self.__ast]
		logging.debug("Current: {0}".format(current[0].value))
		is_first = [False]

		f = open(config_file)
		contents = f.read()
		f.close()

		config_tree <<= +s_expression

		s_expression <<= -Character("(")(lambda match: configuration.__set_first(is_first)) \
						<< +(-value | s_expression) \
						<< -Character(")")(lambda match: configuration.__step_up(current))

		value <<= Text()(lambda match: configuration.__add_child_and_step(current, match[1:-1], is_first)) \
				| Number()(lambda match: configuration.__add_child_and_step(current, match, is_first)) \
				| Identifier()(lambda match: configuration.__add_child_and_step(current, match, is_first))

		r = ResultRange()
		success = BaseRule.parse(config_tree, contents, r)

		if not success:
			raise ValueError("{0} is not a conforming configuration file:\n{1}".format(config_file, BaseRule.get_last_error().get_error_message(contents)))

	def get_child(self, config_path, separator = "."):
		# throws ValueError if the path is not valid
		path = config_path.split(separator)
		current_node = self.__ast

		for p in path:
			current_node = current_node.get_child(p)

		return current_node

	def get(self, config_path, separator = "."):
		return self.get_child(config_path, separator).children[0]

	def get_valuelist(self, config_path, separator = "."):
		return [str(value) for value in self.get_child(config_path, separator).children]

	def get_valuepairs(self, config_path, separator = "."):
		child = self.get_child(config_path, separator)
		result = []

		for c in child.children:
			result.append( (c.value, c.children[0]) )

		return result

	@staticmethod
	def __add_child_and_step(current, value, is_first):
		current[0].add_child(value)
		if is_first[0]:
			current[0] = current[0].children[-1]
			is_first[0] = False

	@staticmethod
	def __step_up(current):
		current[0] = current[0].parent

	@staticmethod
	def __set_first(is_first):
		is_first[0] = True

def test0():
	c = configuration("test_data/config")

	print(c.get("compiler.flags.output"))
	print(c.get("directories.external_source"))
	print(c.get("system_libraries:math.h", ":"))
	print(c.get("compiler.optimization_level"))
	print(" ".join(c.get_valuelist("compiler.flags.warning")))

	for key, value in c.get_valuepairs("system_libraries"):
		print("Key: {0}\tValue: {1}".format(key, value))

	return True

def test1():
	try:
		c = configuration("test_data/config_error")
	except ValueError as ve:
		print(ve)
		return True
	
	return False

def main():
	tests = [test0, test1]

	t = tester.Tester(tests)

	return t.run()

main()
