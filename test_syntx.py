import sys
import logging

import tester
from syntx import *

def test0():
	a = Rule("compound1")

	a <<= Character("abc")(lambda match: print("Matched: {0}".format(match)))

	t = "b"
	r = ResultRange()
	success = BaseRule.parse(a, t, r)

	if success:
		print("Success: {0} ({1}, {2})".format(t[r.start:r.end], r.start, r.end))
		return True
	else:
		print("Didn't match")
		return False

def test1():
	a = Rule("compound1")

	a <<= Character("abc") << Character("def")(lambda match: print("Matched: {0}".format(match)))

	t = "bf"
	r = ResultRange()
	success = BaseRule.parse(a, t, r)

	if success:
		print("Success: {0} ({1}, {2})".format(t[r.start:r.end], r.start, r.end))
		return True
	else:
		print("Didn't match")
		return False

def test2():
	a = Rule("compound1")
	b = Rule("compound2")
	
	b <<= Character("abc") << (Character("def") | Character("ghi")) << -a
	a <<= Option(Character("jkl"))

	t = "ah j"
	r = ResultRange(3,4)
	success = BaseRule.parse(b, t, r)

	if success:
		print("Success: {0} ({1}, {2})".format(t[r.start:r.end], r.start, r.end))
		return True
	else:
		print("Didn't match")
		return False

def test3():
	line = Rule("line")

	line <<= Text("\"", "\\") << Character(".")

	t = "\"hello world\"."
	r = ResultRange()
	success = BaseRule.parse(line, t, r)

	if success:
		print("Success: {0} ({1}, {2})".format(t[r.start:r.end], r.start, r.end))
		return True
	else:
		print("Didn't match")
		return False

def test4():
	s_expression = Rule("s_expression")

	s_expression <<= -Character("(") << +(-Text() | s_expression) << -Character(")")

	t = "(\"alma\" (\"citrom\"\n\t\"narancs\") \"dio\")"
	r = ResultRange()
	success = BaseRule.parse(s_expression, t, r)

	if success:
		print("Success: {0} ({1}, {2})".format(t[r.start:r.end], r.start, r.end))
		return True
	else:
		print("Didn't match")
		return False

def test5():
	s_expression = Rule("s_expression")
	value = Rule("value")

	s_expression <<= -Character("(") << +(-value | s_expression) << -Character(")")
	value <<= Text() | Number() | Identifier()

	t = "(\"alma\" (2 citrom\n\t\"narancs\") \"dio\" 567 mazsola3 (-23 +4.87 (key (value1 5.6 \"value2\") )) )"
	r = ResultRange()
	success = BaseRule.parse(s_expression, t, r)

	if success:
		print("Success: {0} ({1}, {2})".format(t[r.start:r.end], r.start, r.end))
		return True
	else:
		print("Didn't match")
		return False

def roam_tree(text, root, tabs = 0):
	if root is not None:
		if root.result is not None:
			spaces = " " * tabs
			print("{0}Operation : {1}\tValue: {2}".format(spaces, root.operation, text[root.result.start : root.result.end]))
		
		for child in root.children:
			tabs += 1
			roam_tree(text, child, tabs)
			tabs -= 1

def test6():
	config_tree = Rule("config_tree")
	s_expression = Rule("s_expression")
	value = Rule("value")

	f = open("test_data/config")
	contents = f.read()
	f.close()

	config_tree <<= +s_expression
	s_expression <<= -Character("(") << +(-value | s_expression) << -Character(")")
	value <<= Text() | Number()(lambda match: print("Number: {0}".format(match))) | Identifier()

	r = ResultRange()
	t = AstNode()
	success = BaseRule.parse(config_tree, contents, r, t)

	if success:
		print("Success: {0} ({1}, {2})".format(contents[r.start:r.end], r.start, r.end))
		roam_tree(contents, t)
		return True
	else:
		print("Didn't match")
		return False

def test7():
	expression = Rule("expression")

	expression <<= -Keyword("if") << -Character("(") << -Identifier() << -Keyword("in") << -Identifier() << -Character(")") << -Keyword("return") << -Keyword("true") << -Epsilon()

	l = "if (apple in fruits) return true\n\t  "
	r = ResultRange()
	t = AstNode()
	success = BaseRule.parse(expression, l, r, t)

	if success:
		print("Success: {0} ({1}, {2})".format(l[r.start:r.end], r.start, r.end))
		roam_tree(l, t)
		return True
	else:
		print("Didn't match")
		return False

class node(object):
	def __init__(self, a_value = "", parent = None):
		self.value = a_value
		self.parent = parent
		self.children = []

	def add_child(self, value = ""):
		self.children.append(node(value, self))

	def get_child(self, value):
		for child in self.children:
			if child.value == value:
				return child

		raise ValueError("{0} is not child of the current node".format(value))

	def __str__(self):
		return self.value

class tree_printer(object):
	def __init__(self):
		self.__counter = 0

	def print_tree(self, node):
		for i in range(self.__counter):
			sys.stdout.write("->")

		print(" \"{0}\"".format(node.value))

		self.__counter += 1

		for child in node.children:
			self.print_tree(child)

		self.__counter -= 1

def add_child_and_step(current, value, is_first):
	current[0].add_child(value)
	if is_first[0]:
		current[0] = current[0].children[-1]
		is_first[0] = False

def step_up(current):
	current[0] = current[0].parent

def set_first(is_first):
	is_first[0] = True

def test8():
	config_tree = Rule("config_tree")
	s_expression = Rule("s_expression")
	value = Rule("value")

	ast = node()
	current = [ast]
	logging.debug("Current: {0}".format(current[0].value))
	is_first = [False]

	f = open("test_data/config")
	contents = f.read()
	f.close()

	config_tree <<= +s_expression
	s_expression <<= -Character("(")(lambda match: set_first(is_first)) << +(-value | s_expression) << -Character(")")(lambda match: step_up(current))
	value <<= Text()(lambda match: add_child_and_step(current, match, is_first)) | Number()(lambda match: add_child_and_step(current, match, is_first)) | Identifier()(lambda match: add_child_and_step(current, match, is_first))

	r = ResultRange()
	success = BaseRule.parse(config_tree, contents, r)

	if success:
		print("Success: {0} ({1}, {2})".format(contents[r.start:r.end], r.start, r.end))

		printer = tree_printer()
		printer.print_tree(ast)
		return True
	else:
		print("Didn't match")
		return False

class tree_roaming(object):
	def __init__(self, text):
		self.tabs = 0
		self.text = text

	def roam_tree(self, root):
		for i in range(self.tabs):
			sys.stdout.write("\t")

		if root is not None:
			if root.result is not None:
				print("Operation : {0}\tValue: {1}".format(root.operation, self.text[root.result.start : root.result.end]))
			
			self.tabs += 1

			for child in root.children:
				self.roam_tree(child)

			self.tabs -= 1

def test9():
	config_tree = Rule("config_tree")
	s_expression = Rule("s_expression")
	value = Rule("value")

	contents = "(alma)"

	config_tree <<= s_expression
	s_expression <<= -Character("(") << +(-value | s_expression) << -Character(")")
	value <<= Text() | Number()(lambda match: print("Number: {0}".format(match))) | Identifier()

	r = ResultRange()
	t = AstNode()
	success = BaseRule.parse(config_tree, contents, r, t)

	if success:
		print("Success: {0} ({1}, {2})".format(contents[r.start:r.end], r.start, r.end))
		tr = tree_roaming(contents)
		tr.roam_tree(t)
		return True
	else:
		print("Didn't match")
		return False

def test10():
	line = Rule("line")

	line <<= -Keyword("if") << -Character("(") << -Identifier() << -Keyword("==") << -Number() << -Character(")")

	t = "if (variable != 2)"
	r = ResultRange()
	success = BaseRule.parse(line, t, r)

	if success:
		print("Matched: {0} ({1}, {2})".format(t[r.start:r.end], r.start, r.end))
		return False
	else:
		print("Didn't match")
		last_error = BaseRule.get_last_error()
		print("Context: {0}".format(t))
		print(last_error.get_error_message(t))
		return True

def test11():
	a = Rule("compound1")
	b = Rule("compound2")

	a <<= (Character("abc") << Character("def"))(lambda match: print("Matched: {0}".format(match)))
	b <<= a(lambda match: print("Compound match: {0}".format(match))) << Character("ghi")

	t = "bfg"
	r = ResultRange()
	success = BaseRule.parse(b, t, r)

	if success:
		print("Success: {0} ({1}, {2})".format(t[r.start:r.end], r.start, r.end))
		return True
	else:
		print("Didn't match")
		return False

def test12():
	word = Rule("word")
	entry = Rule("entry")

	word <<= Identifier("æøå")
	entry <<= +-word

	t = "æblerne er røde"
	r = ResultRange()
	success = BaseRule.parse(entry, t, r)

	if success:
		print("Success: {0} ({1}, {2})".format(t[r.start:r.end], r.start, r.end))
		return True
	else:
		print("Didn't match")
		return False

def test13():
	word = Rule("word")

	word <<= +CRange("A", "F") << Character("Q")

	t = "DABEQ"
	r = ResultRange()
	success = BaseRule.parse(word, t, r)

	if success:
		print("Success: {0} ({1}, {2})".format(t[r.start:r.end], r.start, r.end))
		return True
	else:
		print("Didn't match")
		return False

def test14():
	word = Rule("word")

	word <<= -Character("$") << Substring("GP") << Substring("GSV");

	t = "$GPGSV"
	r = ResultRange()
	success = BaseRule.parse(word, t, r)

	if success:
		print("Success: {0} ({1}, {2})".format(t[r.start:r.end], r.start, r.end))
		return True
	else:
		print("Didn't match")
		return False

def test15():
	regex = Rule("regex")

	regex <<= Keyword("def") << -Identifier("_") << Character("(") << Option(-Identifier("_") << Option(-Character(",") << -Identifier("_"))) << -Keyword("):")

	text = """
		def alma():
			bla()

		def korte(egy, ketto):
			bla_bla()

		def hello_vilag(szilva):
			print("hello vilag!")
		"""

	success = False
	for match in BaseRule.search(regex, text):
		success = True
		print("Match: {0}".format(text[match.start : match.end]))

	return success

def test16():
	regex = Rule("regex")
	argument_list = Rule("argument_list")

	regex <<= Keyword("def") << -Identifier("_") << Character("(") << -argument_list << -Keyword("):")
	argument_list <<= Option(-Identifier("_") << Option(-Character(",") << -Identifier("_")))

	text = """
		def alma():
			bla()

		def korte(egy, ketto):
			bla_bla()

		def hello_vilag(szilva):
			print("hello vilag!")
		"""

	success = False
	for match in BaseRule.search(regex, text):
		success = True
		print("Match: {0}".format(text[match.start : match.end]))

	return success

def test17():
	regex = Rule("regex")
	argument_list = Rule("argument_list")

	regex <<= Keyword("def") << -Identifier("_") << Character("(") << -argument_list << -Keyword("):")
	argument_list <<= Option(-Identifier("_") << Option(-Character(",") << -Identifier("_")))

	text = """
		def alma():
			bla()

		def korte(egy, ketto):
			bla_bla()

		def hello_vilag(szilva):
			print("hello vilag!")
		"""

	success = False
	
	iterator = BaseRule.search(regex, text)

	for match in iterator:
		success = True
		print("Match: {0}".format(text[match.start : match.end]))

	for match in iterator:
		success = True
		print("Match: {0}".format(text[match.start : match.end]))

	return success

def test18():
	regex = Rule("regex")
	argument_list = Rule("argument_list")

	regex <<= Keyword("def") << -Identifier("_") << Character("(") << -argument_list << -Substring("):")
	argument_list <<= Option(-Identifier("_") << Option(-Character(",") << -Identifier("_")))

	text = """
		def alma():def korte(egy, ketto):
			bla_blaV()

		def hello_vilag(szilva):
			print("hello vilag!")
		"""

	success = False
	
	iterator = BaseRule.search(regex, text)

	for match in iterator:
		success = True
		print("Match: {0}".format(text[match.start : match.end]))

	return success

def test19():
	regex = Rule("regex")
	argument_list = Rule("argument_list")

	regex <<= Keyword("def") << -Identifier("_") << Character("(") << -argument_list << -Substring("):")
	argument_list <<= Option(-Identifier("_") << Option(-Character(",") << -Identifier("_")))

	text = """
		def alma():def korte(egy, ketto):
			bla_blaV()

		def hello_vilag(szilva):
			print("hello vilag!")
		"""

	success = False
	
	matches = []
	for match in BaseRule.search(regex, text):
		success = True
		matches.append(match)

	replace = "<<replaced>>"
	result = ""
	previous = None
	for m in matches:
		result += text[previous.end : m.start] if previous is not None else text[:m.start]
		result += replace
		previous = m

	print(result)

	return success

def test20():
	regex = Rule("regex")

	regex <<= Keyword("sub") << ~(Identifier("_") ^ "name") << ~Character("(") << ~Character(")")

	text = " sub my_fun()\n\tsub other()"

	success = False
	for match in BaseRule.search(regex, text):
		if Rule.get_group("name") is not None:
			print("Match: {0}\tGroup(name): {1}".format(text[match.start : match.end], " ".join(Rule.get_group("name"))))
			success = True

	return success

def test21():
	regex = Rule("regex")

	regex <<= Keyword("sub") << ~(Identifier("_") ^ "name") << ~Character("(") << Option(Identifier("_") ^ "argument")<< ~Character(")")

	text = " sub my_fun(alma)\n{print(\"hello\");}\n\tsub other() {print(\"other\");}"

	result = BaseRule.replace(regex, text, "function __{name}__\{{argument}\}")

	if result == "": return False
	else:
		print("Result: {0}".format(result))
		return True 

def test22():
	rule1 = Rule("rule1")

	rule1 <<= Keyword("apple") << Option(~Identifier()) << (~Character(":") | ~Number()) << Character("#") << -Epsilon()

	text = "apple tree :# "
	r = ResultRange()
	t = AstNode()

	success = BaseRule.parse(rule1, text, r, t)

	if success:
		print("Success: {0} ({1}, {2})".format(text[r.start:r.end], r.start, r.end))
		roam_tree(text, t)
		return True
	else:
		print("Didn't match")
		last_error = BaseRule.get_last_error()
		print("Context: {0}".format(text))
		print(last_error.get_error_message(text))
		return False

def test23():
	regex = Rule("regex")
	argument_list = Rule("argument_list")
	argument = Rule("argument")

	regex <<= Keyword("sub") << ~(Identifier("_") ^ "name") << ~Character("(") << Option(argument_list)<< ~Character(")")
	argument_list <<= ~argument << Kleene(~Character(",") << ~argument)
	argument <<= Identifier("_") ^ "argument"

	text = " sub my_fun(alma, citrom, narancs)\n{print(\"hello\");}\n\tsub other() {print(\"other\");}"

	result = BaseRule.replace(regex, text, "function __{name}__\{{(##)argument}\}")

	if result == "": return False
	else:
		print("Result: {0}".format(result))
		return True 

def test24():
	expression = Rule("expression")
	expression <<= Kleene(~Identifier()) << ~Character("(") << Kleene(~Number()) << ~Character(")") << -Epsilon()
	text = "apple tree ()\t\n"
	r = ResultRange()
	t = AstNode()

	success = BaseRule.parse(expression, text, r, t)

	if success:
		print("Success: {0} ({1}, {2})".format(text[r.start:r.end], r.start, r.end))
		roam_tree(text, t)
		return True
	else:
		print("Didn't match")
		last_error = BaseRule.get_last_error()
		print("Context: {0}".format(text))
		print(last_error.get_error_message(text))
		return False

def test25():
	url = Rule("url")
	protocol = Rule("protocol")
	path = Rule("path")

	url <<= ~(protocol ^ "prot") << path
	protocol <<= Identifier() << Substring("://")
	path <<= +((Identifier("_.") ^ "pathelement") << Kleene(Character("\//")))

	text = r"   http://svn/svn\\/\code//app////iGO\/BRANCHES// valami szöveg https://inside.nng.com/\\/valami/meg/mas.html és még egy kis szöveg"

	result = BaseRule.replace(url, text, "{prot}{(/)pathelement}")

	if result == "": return False
	else:
		print("Result: {0}".format(result))
		return True 

def test26():
	separator = Rule("separator")
	separator <<= (NotCharacter(":") ^ "char") << +Character("\//")

	text = r"   http://svn/svn\\/\code//app////iGO\/BRANCHES// valami szöveg https://inside.nng.com/\\/valami/meg/mas.html és még egy kis szöveg"

	result = BaseRule.replace(separator, text, "{char}/")

	if result == "": return False
	else:
		print("Result: {0}".format(result))
		return True 

def test27():
	expression = Rule("expression")
	expression <<= Kleene(~Identifier()) << ~Character("(") << Option(~NotCRange("a", "z")) << ~Character(")") << -Epsilon()
	text = "apple tree ( A )\t\n"
	r = ResultRange()
	t = AstNode()

	success = BaseRule.parse(expression, text, r, t)

	if success:
		print("Success: {0} ({1}, {2})".format(text[r.start:r.end], r.start, r.end))
		roam_tree(text, t)
		return True
	else:
		print("Didn't match")
		last_error = BaseRule.get_last_error()
		print("Context: {0}".format(text))
		print(last_error.get_error_message(text))
		return False

def test28():
	expression = Rule("expression")
	expression <<= +-Number()
	text = "5.367 -4.9 3"
	r = ResultRange()

	success = BaseRule.parse(expression, text, r)

	if success:
		print("Success: {0} ({1}, {2})".format(text[r.start:r.end], r.start, r.end))
		return True
	else:
		print("Didn't match")
		return False

def test29():
	expression = Rule("expression")
	expression <<= Text("#", "&") 
	text = "#hello &# world#"
	r = ResultRange()

	success = BaseRule.parse(expression, text, r)

	if success:
		print("Success: {0} ({1}, {2})".format(text[r.start:r.end], r.start, r.end))
		return True
	else:
		print("Didn't match")
		return False

def test30():
	if_clause = Rule("if_clause")
	head = Rule("head")
	condition = Rule("condition")

	if_clause <<= -Identifier() << head 
	head <<= -Character("(") << -condition << -Character(")")
	condition <<= -Identifier("$") << -Character("<") << -Number()

	text = "if ($apple < 3.9)"
	r = ResultRange()
	success = BaseRule.parse(if_clause, text, r)

	if success:
		print("Success: {0} ({1}, {2})".format(text[r.start:r.end], r.start, r.end))
		return True
	else:
		print("Didn't match")
		return False

def test31():
	expression = Rule("expression")
	expression <<= Keyword("ap_p!e") << -Character("abcdefghi")
	text = "ap_p!e g"
	r = ResultRange()

	success = BaseRule.parse(expression, text, r)

	if success:
		print("Success: {0} ({1}, {2})".format(text[r.start:r.end], r.start, r.end))
		return True
	else:
		print("Didn't match")
		return False

def test32():
	expression = Rule("expression")
	expression <<= +NotCharacter("#") << Character("#")
	text = "apple lemon orange#"
	r = ResultRange()

	success = BaseRule.parse(expression, text, r)

	if success:
		print("Success: {0} ({1}, {2})".format(text[r.start:r.end], r.start, r.end))
		return True
	else:
		print("Didn't match")
		return False

def test33():
	expression = Rule("expression")
	expression <<= Keyword("$apple", "_")
	text = "$apple_tree"
	r = ResultRange()

	success = BaseRule.parse(expression, text, r)

	if success:
		print("False match: {0} ({1}, {2})".format(text[r.start:r.end], r.start, r.end))
		return False
	else:
		print("Didn't match -- which is good")
		return True

def test34():
	expression = Rule("expression")
	expression <<= +-(Character("(") << -CRange("a", "z") << Character(")")) << -Epsilon()
	text = "(a) (b) ()   \t"
	r = ResultRange()

	success = BaseRule.parse(expression, text, r)

	if success:
		if r.end == len(text):
			print("Full match: {0} ({1}, {2})".format(text[r.start:r.end], r.start, r.end))
			return False
		else:
			print("Partial match: {0} ({1}, {2})".format(text[r.start:r.end], r.start, r.end))
			return True
	else:
		print("Didn't match")
		return False

def test35():
	expression = Rule("expression")
	expression <<= +-(Character("(") << -CRange("a", "z") << Character(")")) << -Epsilon()
	text = "(a) (b) (i)   \t"
	r = ResultRange()

	success = BaseRule.parse(expression, text, r)

	if success:
		if r.end == len(text):
			print("Full match: {0} ({1}, {2})".format(text[r.start:r.end], r.start, r.end))
			return True
		else:
			print("Partial match: {0} ({1}, {2})".format(text[r.start:r.end], r.start, r.end))
			return False
	else:
		print("Didn't match")
		return False

def test36():
	rule1 = Rule("rule1")
	rule1 <<= Character("abc") << Option(CRange("d", "f"))
	text = "af"
	r = ResultRange()
	root = AstNode()

	success = BaseRule.parse(rule1, text, r, root)

	if success:
		print("Matched\nTree:\n")
		roam_tree(text, root)
		return True
	else:
		print("Didn't match")
		return False

def test37():
	rule1 = Rule("rule1")
	rule1 <<= Character("abc") << Kleene(CRange("d", "f"))
	text = "afdde"
	r = ResultRange()
	root = AstNode()

	success = BaseRule.parse(rule1, text, r, root)

	if success:
		print("Matched\nTree:\n")
		roam_tree(text, root)
		return True
	else:
		print("Didn't match")
		return False

def test38():
	rule1 = Rule("rule1")
	rule2 = Rule("rule2")

	rule1 <<= -Character("abc") << +rule2
	rule2 <<= CRange("d", "f") | Character("0123")
	text = "\tafdd1e"
	r = ResultRange()
	root = AstNode()

	success = BaseRule.parse(rule1, text, r, root)

	if success:
		print("Matched\nTree:\n")
		roam_tree(text, root)
		return True
	else:
		print("Didn't match")
		return False

def test39():
	rule1 = Rule("rule1")

	rule1 <<= -Character("abc") << +(CRange("d", "f") | Character("0123"))
	text = "\tafdd1e"
	r = ResultRange()
	root = AstNode()

	success = BaseRule.parse(rule1, text, r, root)

	if success:
		print("Matched\nTree:\n")
		roam_tree(text, root)
		return True
	else:
		print("Didn't match")
		return False

def test40():
	rule1 = Rule("rule1")
	repeated = Rule("repeated")

	rule1 <<= -Character("abc") << repeated
	repeated <<= +(CRange("d", "f") | Character("0123"))
	text = "\tafdd1e"
	r = ResultRange()
	root = AstNode()

	success = BaseRule.parse(rule1, text, r, root)

	if success:
		print("Matched\nTree:\n")
		roam_tree(text, root)
		return True
	else:
		print("Didn't match")
		return False

def test41():
	regex = Rule("regex")
	country_code = Rule("country_code")
	city_code = Rule("city_code")
	telephone_number = Rule("telephone_number")
	digit = Rule("digit")

	regex <<= country_code << -city_code << -telephone_number
	country_code <<= Character("+") << +CRange("0", "9")
	city_code <<= +CRange("0", "9")
	telephone_number <<= digit << +(digit | Character("-"))
	digit <<= CRange("0", "9")

	text = """
		Anna Apple: +48 543 123-456
		Cheryl Cherry: +12 5 12-34-56
		Gina Grape: +34 12 1234-6789-7891
		Lorry Lemon: +567 1 12-45-789
		"""

	success = False
	for match in BaseRule.search(regex, text):
		success = True
		print("Match: {0}".format(text[match.start : match.end]))

	return success

def test42():
	smiley = Rule("smiley")

	smiley <<= Character(":") \
			<< Option(Character("-")) \
			<< (Character(")") | Character("D") | Character("o"))

	text = """
		Sed ut perspiciatis unde :) omnis iste natus error sit :-o voluptatem accusantium doloremque laudantium,
		totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta
		sunt explicabo. Nemo enim ipsam voluptatem quia voluptas :D sit aspernatur aut odit aut fugit, sed quia
		consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui
		dolorem ipsum quia dolor sit amet, consectetur, adipisci :o velit, sed quia non numquam eius modi tempora
		incidunt ut labore et dolore magnam :-) aliquam quaerat voluptatem. :D :D :D
		"""

	success = False
	for match in BaseRule.search(smiley, text):
		success = True
		print("Match: {0}".format(text[match.start : match.end]))

	return success

def test43():
	saddy = Rule("saddy")
	
	saddy <<= Character(":") << Character("(")

	text = "I'm :( surrounded by sad :( faces! :( :("

	result = BaseRule.replace(saddy, text, ":)")

	if result == "": return False
	else:
		print("Result: {0}".format(result))
		return True 

def test44():
	fun_head = Rule("fun_head")
	fun_arg = Rule("fun_arg")

	fun_head <<= Keyword("def") << ~(Identifier("_") ^ "name") << fun_arg << Character(":")
	fun_arg <<= ~Character("(") << Option(Identifier("_") ^ "argument")<< ~Character(")")

	text = """
		def my_fun():
			print("my_fun")
	"""

	result = BaseRule.replace(fun_head, text, "function {name}({argument}) \{")

	if result == "": return False
	else:
		print("Result: {0}".format(result))
		return True 

def test45():
	fun_head = Rule("fun_head")
	fun_arg = Rule("fun_arg")
	fun_name = Rule("fun_name")
	arg = Rule("arg")
	args = Rule("args")

	fun_head <<= Keyword("def") << fun_name << fun_arg << Character(":")
	fun_name <<= ~(Identifier("_") ^ "name")
	fun_arg <<= ~Character("(") << args << ~Character(")")
	args <<= -arg << Kleene(Character(",") << -arg)
	arg <<= Option(Identifier("_") ^ "argument")

	text = """
		def my_fun(apple, lemon, cheesburger):
			print("my_fun")
	"""

	result = BaseRule.replace(fun_head, text, "function {name}({(; )argument}) \{")

	if result == "": return False
	else:
		print("Result: {0}".format(result))
		return True 

def test46():
	number = Rule("number")

	number <<= Character("[") << Number()

	text = "[2341"
	r = ResultRange()

	success = BaseRule.parse(number, text, r)

	if success:
		print("Matched: {0} ({1}, {2})".format(text[r.start:r.end], r.start, r.end))
		return True
	else:
		print("Didn't match")
		last_error = BaseRule.get_last_error()
		print("Context: {0}".format(text))
		print(last_error.get_error_message(text))
		return False

def test47():
	rule_a = Rule("rule_a")
	rule_b = Rule("rule_b")

	rule_a <<= Identifier() << ~(rule_b ^ "rule_b") << ~Number() << ~rule_b
	rule_b <<= Text()

	text = "apple \"lemon\" 12.3 \"orange\""
	result = ResultRange()

	success = BaseRule.parse(rule_a, text, result)

	if success:
		for match in BaseRule.get_group("rule_b"):
			print(match)
		return True

	return False

def test48():
	rule_a = Rule("rule_a")
	rule_b = Rule("rule_b")

	rule_a <<= Identifier() << ~(~rule_b ^ "rule_b") << ~Number() << ~rule_b
	rule_b <<= Text()

	text = "apple \"lemon\" 12.3 \"orange\""
	result = ResultRange()

	success = BaseRule.parse(rule_a, text, result)

	if success:
		for match in BaseRule.get_group("rule_b"):
			print(match)
		return True

	return False

def test49():
	rule = Rule("rule")

	rule <<= Identifier()(lambda match: print(match)) << Number()

	result = BaseRule()
	text = "apple lemon"

	success = BaseRule.parse(rule, text, result)

	if success:
		return False
	else:
		return True

def main():
	#logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
	
	tests = [
				test0, test1, test2, test3, test4, test5, test6, test7, test8, test9, test10,
				test11, test12, test13, test14, test15, test16, test17, test18, test19, test20,
				test21, test22, test23, test24, test25, test26, test27, test28, test29, test30,
				test31, test32, test33, test34, test35, test36, test37, test38, test39, test40,
				test41, test42, test43, test44, test45, test46, test47, test48, test49
			]

	the_tester = tester.Tester(tests)
	return the_tester.run()

main()

